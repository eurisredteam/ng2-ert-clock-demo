import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { ErtClockModule } from 'ng2-ert-clock';
import { AppComponent } from './app.component';

@NgModule({
    imports: [
        BrowserModule,
        ErtClockModule
    ],
    declarations: [
        AppComponent
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
