# ERT-CLOCK for Angular 2 - Demo

## Overview
Demo for ERT-CLOCK: an Angular 2 component that shows a clock set on the timezone provided as component input.  
That component time is Daylight Saving Time aware and AoT ready.


## How to use it

0. Clone this repository and open terminal on the project folder
1. Install dependencies with <code>npm install</code>

2. Launch:
    * JiT version => launch with <code>npm start</code>
    * AoT + tree-shake version => build with <code>npm run build:prod</code> and move the contents of <code>_dist</code> folder on your web server


## Notes
The build process illustrated in '2b' creates, in the <code>_dist</code> folder, only the js bundle and index.html.

Therefore if you modify the styles.css or you need other assets (quadrant image for example) you will have to manually copy it to <code>_dist</code> folder or web server folder.

For the sake of the demo the <code>_dist</code> folder initally already contains the default css and quadrant, together with the output of compilation described in '2b' (so you can move to your web server folder and try it).


## Questions?
Feel free to contact the author!

