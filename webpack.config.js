const webpack = require('webpack');
const HtmlWebpackPlugin = require("html-webpack-plugin");
var ngToolsWebpack = require('@ngtools/webpack');

const isProd = process.argv.indexOf('-p') !== -1;
console.log(isProd ? "PRODUCTION" : "DEVELOPMENT");

const plugins = [
    // see https://github.com/angular/angular/issues/11580
    new webpack.ContextReplacementPlugin(
        /angular(\\|\/)core(\\|\/)(esm(\\|\/)src|src)(\\|\/)linker/,
        './src'
    ),
    //new webpack.NamedModulesPlugin()
    new HtmlWebpackPlugin({ template: __dirname + '/src/index.html' })
];

if (isProd) {
    plugins.push(
        new webpack.DefinePlugin({
            'process.env': {
                'NODE_ENV': `"production"`
            }
        })
        , new webpack.optimize.UglifyJsPlugin({
            beautify: false,
            mangle: {
                screw_ie8: true,
                keep_fnames: true
            },
            compress: {
                warnings: false,
                screw_ie8: true
            },
            comments: false
        })
        , new ngToolsWebpack.AotPlugin({
            tsConfigPath: __dirname + '/tsconfig.webpack-prod.json',
            entryModule: __dirname + '/src/app/app.module#AppModule'
        })
    );
}
else {
    plugins.push(
        new webpack.DefinePlugin({
            'process.env': {
                'NODE_ENV': `"development"`
            }
        })
        , new webpack.LoaderOptionsPlugin({
            minimize: false,
            debug: true
        })
    );
}

const rules = [
    {
        test: /\.ts$/,
        loaders: isProd ? '@ngtools/webpack' : ['awesome-typescript-loader', 'angular2-template-loader'],
    },
    { test: /\.html$/, loader: 'raw-loader' },
    { test: /\.css$/, loader: 'raw-loader' },
    {
        test: /\.json$/,
        loaders: ["json-loader"]
    }
];

const resolve = {
    extensions: ['.js', '.ts', '.html', '.css']
};

module.exports = {
    //devtool: isProd ? 'source-map' : 'eval',
    entry: __dirname + '/src/app/main.ts',
    output: {
        filename: 'bundle.js',
        path: __dirname + '/_dist'
    }
    , module: { rules: rules }
    , resolve: resolve
    , devServer: {
        historyApiFallback: true
    }
    , plugins: plugins
};